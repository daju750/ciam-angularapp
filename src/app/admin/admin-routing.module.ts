import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { PacienteComponent } from './components/paciente/paciente.component';
import { NuevoComponent } from './pages/nuevo/nuevo.component';
import { ListaPacientesComponent } from './pages/lista-pacientes/lista-pacientes.component';
import { ConsultaPacienteComponent } from './pages/consulta-paciente/consulta-paciente.component';
import { ErrorComponent } from './pages/error/error.component';

const routes: Routes = [
  {
    path:'',
    component: MainComponent,
      children:[
        {path:'dashboard',component:DashboardComponent},
        {path:'registro',component:RegistroComponent},
        {path:'paciente/nuevo',component:NuevoComponent},
        {path:'paciente/lista',component:ListaPacientesComponent},
        {path:'paciente/consulta/:id',component:ConsultaPacienteComponent},
        {path:'error',component:ErrorComponent},
        {path:'**',component:DashboardComponent}
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
