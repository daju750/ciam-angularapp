import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { MainComponent } from './pages/main/main.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { PacienteComponent } from './components/paciente/paciente.component';
import { NuevoComponent } from './pages/nuevo/nuevo.component';

import { ReactiveFormsModule } from '@angular/forms';
import { ListaPacientesComponent } from './pages/lista-pacientes/lista-pacientes.component';
import { ConsultaPacienteComponent } from './pages/consulta-paciente/consulta-paciente.component';
import { ErrorComponent } from './pages/error/error.component';

@NgModule({
  declarations: [
    MainComponent,
    DashboardComponent,
    RegistroComponent,
    PacienteComponent,
    NuevoComponent,
    ListaPacientesComponent,
    ConsultaPacienteComponent,
    ErrorComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }
