import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PersonaService } from '../../services/persona.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Persona } from '../../interfaces/interfaces';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit{

  constructor(private actroute: ActivatedRoute,private fb:FormBuilder,private router:Router,private personaService: PersonaService){}

  DataPersona!:Persona;
  id!:string;

  Formulario: FormGroup = this.fb.group({
    nombres_completos:['',Validators.required],
    apellidos_completos:['',[Validators.required]],
    identificacion:['',[Validators.required,Validators.minLength(13)]],
    tipo_identificacion:['',[Validators.required]],
    fecha_nacimiento:['',[Validators.required]],
    direccion:['',[Validators.required]],
    genero:['',[Validators.required]],
    departamento:['',[Validators.required]],
    municipio:['',[Validators.required]],
    telefono:['',[Validators.required,Validators.minLength(12)]],
    tipo_paciente:['',[Validators.required]],
    rol:['PACIENTE_POTENCIAL_ROL',[Validators.required]],
    email:['',[Validators.required]],
  })

  ngOnInit(): void {
    
    this.id = this.actroute.snapshot.paramMap.get('id')!;
    if((Number(this.id))){
      this.personaService.ConsultaPersona(this.id!).subscribe(
        persona => {
          if(!persona){
            this.router.navigateByUrl("/admin/error");
          }else{
            this.DataPersona = persona;
            console.log(this.DataPersona)
            this.Formulario.controls['nombres_completos'].setValue(this.DataPersona.nombres_completos);
            this.Formulario.controls['apellidos_completos'].setValue(this.DataPersona.apellidos_completos);
            this.Formulario.controls['identificacion'].setValue(this.DataPersona.identificacion);
            this.Formulario.controls['tipo_identificacion'].setValue(this.DataPersona.tipo_identificacion);
            this.Formulario.controls['fecha_nacimiento'].setValue(this.DataPersona.fecha_nacimiento);
            this.Formulario.controls['direccion'].setValue(this.DataPersona.direccion);
            this.Formulario.controls['genero'].setValue(this.DataPersona.genero);
            this.Formulario.controls['departamento'].setValue(this.DataPersona.departamento);
            this.Formulario.controls['municipio'].setValue(this.DataPersona.municipio);
            this.Formulario.controls['telefono'].setValue(this.DataPersona.telefono);
            this.Formulario.controls['tipo_paciente'].setValue(this.DataPersona.tipo_paciente);
            this.Formulario.controls['rol'].setValue(this.DataPersona.rol);
            this.Formulario.controls['email'].setValue(this.DataPersona.email);
          }
        }
      );
    }else{
      this.router.navigateByUrl("/admin/error");
    }
  }

  FormActulizarPersona(){
    this.DataPersona = this.Formulario.value;
    this.DataPersona.id = this.id;
    this.personaService.ActulizarPersona(this.DataPersona).subscribe(
      ok =>{
        if(ok){
          window.location.reload();
          Swal.fire('Actulizacion Exitoso','Todo Salio Bien!','success')
        }else{
          Swal.fire('Error','Creadenciales Invalidas','error')
          console.log(ok);
        }
      }
    );
  }

}
