export interface Persona {
    id?:string,
    ok?:boolean,
    nombres_completos?:string,
    apellidos_completos?:string,
    identificacion?:string,
    tipo_identificacion?:string,
    fecha_nacimiento?:string,
    direccion?:string,
    genero?:string,
    departamento?:string,
    municipio?:string,
    telefono?:string,
    tipo_paciente?:string,
    rol?:string,
    email?:string,
}


export interface Rol {
    id:string,
    nombre:string
}

export interface EncabezadoPersona{
    id:string,
    identificacion?:string,
    tipo_identificacion?:string,
    nombres_completos?:string,
    apellidos_completos?:string,
    email?:string
}