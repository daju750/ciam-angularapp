import { Component, OnInit } from '@angular/core';
import { EncabezadoPersona } from '../../interfaces/interfaces';
import { PersonaService } from '../../services/persona.service';
import { map } from 'rxjs';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-pacientes',
  templateUrl: './lista-pacientes.component.html',
  styleUrls: ['./lista-pacientes.component.css']
})
export class ListaPacientesComponent implements OnInit{
  
  public encabezadoLista:Array<EncabezadoPersona> = [];

  constructor(private personaService:PersonaService,private router:Router){}

  ngOnInit(): void {
    
    this.personaService.ListarPersona().pipe(
      map(resp=> {
        this.encabezadoLista = resp;
        console.log(this.encabezadoLista);
      })
    ).subscribe();
  }

  async eliminar(id:string){

    Swal.fire({
      title: 'Esta seguro?',
      text: "Una vez eliminador no se puede recuperar!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, proceder!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.personaService.EliminarPersona(id).subscribe(
          ok =>{
            if(ok){
              window.location.reload();
              Swal.fire('Eliminacion Exitoso','Todo Salio Bien!','success')
            }else{
              Swal.fire('Error','Creadenciales Invalidas','error')
            }
          }
        );
      }
    })
  }

}
