import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Persona } from '../../interfaces/interfaces';
import { PersonaService } from '../../services/persona.service';
import { AuthService } from 'src/app/auth/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})

export class NuevoComponent {

  constructor(private fb:FormBuilder,private router:Router,private personaService: PersonaService,private auth:AuthService){}

  Formulario: FormGroup = this.fb.group({
    nombres_completos:['Juan Manuel',Validators.required],
    apellidos_completos:['Perez Obrador',[Validators.required]],
    identificacion:['2138456890504',[Validators.required,Validators.minLength(13)]],
    tipo_identificacion:['DPI',[Validators.required]],
    fecha_nacimiento:['15/06/1993',[Validators.required]],
    direccion:['5ave 5-18 Zona 12',[Validators.required]],
    genero:['MASCULINO',[Validators.required]],
    departamento:['Escuintla',[Validators.required]],
    municipio:['Escuintla',[Validators.required]],
    telefono:['53147521',[Validators.required,Validators.minLength(12)]],
    tipo_paciente:['ESTANDAR',[Validators.required]],
    rol:['PACIENTE_POTENCIAL_ROL',[Validators.required]],
    email:['juanm@gmail.com',[Validators.required]],
  })

  persona!:Persona;

  FormPersona(){
    this.persona = this.Formulario.value;
    this.personaService.NuevaPersona(this.persona).subscribe(
      ok =>{
        if(ok){
          Swal.fire('Ingreso Exitoso','Todo Salio Bien!','success')
          this.router.navigateByUrl('/admin/registro');
        }else{
          Swal.fire('Error','Creadenciales Invalidas','error')
        }
      }
    );
  }

}
