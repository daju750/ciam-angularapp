import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/app/environments/environments';
import { EncabezadoPersona, Persona} from '../interfaces/interfaces';
import { Observable, catchError,of} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  constructor(private http:HttpClient){}

  private baseUrl:string = environment.urlBase;

  NuevaPersona(person:Persona){
    const url = this.baseUrl+"/persona/nueva";
    const body = person;
    const headers = new HttpHeaders({'Authorization': 'Bearer '+localStorage.getItem('token')||''})
    return this.http.post(url,body,{headers}).pipe(catchError(err => of(false)));
  }

  ListarPersona(): Observable<[EncabezadoPersona]>{
    const url = this.baseUrl+"/persona/lista";
    const headers = new HttpHeaders({'Authorization': 'Bearer '+localStorage.getItem('token')||''})
    return this.http.get<[EncabezadoPersona]>(url,{headers})
  }

  EliminarPersona(id:string){
    const url = this.baseUrl+"/persona/eliminar/"+id;
    const headers = new HttpHeaders({'Authorization': 'Bearer '+localStorage.getItem('token')||''})
    return this.http.delete(url,{headers});
  }

  ConsultaPersona(id:string){
    const url = this.baseUrl+"/persona/buscar/"+id;
    const headers = new HttpHeaders({'Authorization': 'Bearer '+localStorage.getItem('token')||''})
    return this.http.get<Persona>(url,{headers});
  }

  ActulizarPersona(persona:Persona){
    const url = this.baseUrl+"/persona/actualizar/"+persona.id;
    const body = persona;
    const headers = new HttpHeaders({'Authorization': 'Bearer '+localStorage.getItem('token')||''})
    return this.http.post(url,body,{headers}).pipe(catchError(err => of(false)));
  }

}
