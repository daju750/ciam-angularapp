import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { validarTokenGuard } from './auth/guards/validar-token.guard';
import { validaLoginGuard } from './auth/guards/validar-login.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () =>import('./auth/auth.module').then(m=>m.AuthModule),
    canActivate:[validaLoginGuard],canLoad:[validaLoginGuard]
  },
  {
    path: 'admin',
    loadChildren: () =>import('./admin/admin.module').then(m=>m.AdminModule),
    canActivate:[validarTokenGuard],canLoad:[validarTokenGuard]
  },
  {
    path: '', redirectTo: '/login', pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: '/login'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
