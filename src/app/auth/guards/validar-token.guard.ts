import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router} from '@angular/router';
import { Observable, tap } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})

export class validarTokenGuard implements CanActivate,CanLoad {

  constructor(private authService:AuthService,private router:Router){}

  canLoad(): Observable<boolean> | boolean {

    const token = localStorage.getItem('token')||'';
    if(token!==''){
      return this.authService.validarToken().pipe(
        tap(valid=>{
          if(!valid){
            this.router.navigateByUrl('/');
            localStorage.removeItem("token")
          }
        })
      );
    }else{
      this.router.navigateByUrl('/login');
      return false;
    }
  }

  canActivate(): Observable<boolean> | boolean {
    
    const token = localStorage.getItem('token')||'';

    if(token!==''){
      return this.authService.validarToken().pipe(
        tap(valid=>{
          if(!valid){
            this.router.navigateByUrl('/');
            localStorage.removeItem("token")
          }
        })
      );
    }else{
      this.router.navigateByUrl('/login');
      return false;
    }
  }
  
};
