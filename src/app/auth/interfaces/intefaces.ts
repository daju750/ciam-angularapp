export interface ResponseLogin{
    ok?:boolean;
    usuario?:string;
    message?:string;
    token?:string;
}

export interface Usuario {
    usuario:string;
    rol?:string;
}