import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private fb:FormBuilder,private router:Router,private authService:AuthService){}

  Formulario: FormGroup = this.fb.group({
    username:['daju150',Validators.required],
    password:['1234',[Validators.required,Validators.minLength(6)]]
  })

  login(){
    
    const{username,password} = this.Formulario.value;
    
    this.authService.login(username,password).subscribe(
      ok =>{
        if(ok){
          this.router.navigateByUrl('/admin/dashboard');
        }else{
          Swal.fire('Error','Creadenciales Invalidas','error')
        }
      }
    );
  }

}
