import { Injectable } from '@angular/core';
import { JwtService } from './jwt.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/app/environments/environments';
import { ResponseLogin, Usuario } from '../interfaces/intefaces';
import { Observable, catchError, map, of, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl:string = environment.urlBase;
  private _usuario!:Usuario;

  get usuario(){
    return {...this._usuario};
  }

  constructor(private http:HttpClient,private jwtServcie:JwtService) {}

  login(username:string,password:string){
    const url = this.baseUrl+"/login";
    const body={username,password};

    return this.http.post<ResponseLogin>(url,body)
      .pipe(
        tap(res=>{
          localStorage.setItem('token',res.token!);
          this._usuario = {
            usuario: res.usuario!
          }
        }),
        catchError(err => of(false))
      );
  }

  validarToken(): Observable<boolean>{
    const url = this.baseUrl+"/jwt";
    const token = localStorage.getItem('token')||'';

    return this.http.post<ResponseLogin>(url,token).pipe(
      map(resp=>{

        this._usuario = {
          usuario: this.jwtServcie.getClaim(token,'sub'),
        }

        return resp.ok!; 
      }),catchError(err=>of(false))
    );
  }

}
